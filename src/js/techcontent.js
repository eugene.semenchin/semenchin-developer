const techcontents = document.querySelector('.techcontents');
const moreBtn = document.querySelector('.more-button');
const text = moreBtn.querySelector('.text');
const arrow = moreBtn.querySelector('.arrow');

moreBtn.addEventListener('click', () => {
    techcontents.classList.toggle('show');
})