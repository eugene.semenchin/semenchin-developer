var swiper = new Swiper(".reviews", {
    effect: "cards",
    grabCursor: true,
    loop: true,
    freeMode: true,
    autoplay: true,
});